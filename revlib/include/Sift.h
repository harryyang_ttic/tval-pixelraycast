#ifndef REVLIB_SIFT_H
#define REVLIB_SIFT_H

#include <string>
#include <vector>
#include "Image.h"
#include "Keypoint.h"

namespace rev {
    
class Sift {
public:
    Sift();
    
    void setParameter(const int octaveTotal, const int levelTotal, const bool preDoubleSize = true);
    void setThreshold(const double peakThreshold, const double edgeThreshold, const double normThreshold);
    
    void detect(const Image<unsigned char>& inputImage,
                std::vector<Keypoint>& keypoints,
                std::vector< std::vector<unsigned char> >& keypointDescriptors);
    void computeKeypointDescriptor(const Image<unsigned char>& inputImage,
                                   const std::vector<Keypoint>& keypoints,
                                   std::vector< std::vector<unsigned char> >& keypointDescriptors);
    
private:
    struct SiftKeypoint {
        int octaveIndex;
        int octaveX;
        int octaveY;
        int octaveLevelIndex;
        
        double x;
        double y;
        double levelIndex;
        double scale;
        std::vector<double> orientations;
    };
    
    void setParameterAll(const Image<unsigned char>& inputImage);
    void buildFirstGaussianOctave(const Image<float>& inputImage);
    void buildGaussianOctave();
    void buildDoGOctave();
    void buildGradientOctave();
    void detectSiftKeypoint(std::vector<SiftKeypoint>& siftKeypoints) const;
    void computeKeypointOrientation(std::vector<SiftKeypoint>& siftKeypoints) const;
    void computeKeypointDescriptor(const std::vector<SiftKeypoint>& siftKeypoints,
                                   std::vector<Keypoint>& keypoints,
                                   std::vector< std::vector<unsigned char> >& keypointDescriptors) const;
    bool convertToSiftKeypoint(const Keypoint& keypoint, SiftKeypoint& siftKeypoint) const;
    
    void upsampleImage(const Image<float>& sourceImage, Image<float>& upsampledImage) const;
    void upsampleRowTranspose(const Image<float>& sourceImage, Image<float>& upsampledHeightImage) const;
    void downsampleImage(const Image<float>& sourceImage, Image<float>& downsampledImage) const;
    bool isLocalMaxima(const int x, const int y, const int levelIndex) const;
    void refineLocalMaxima(const int x, const int y, const int levelIndex,
                           std::vector<SiftKeypoint>& siftKeypoints) const;
    void computeOrientation(SiftKeypoint& siftKeypoint) const;
    bool computeDescriptor(const SiftKeypoint& siftKeypoint, const int orientationIndex,
                           std::vector<unsigned char>& siftDescriptor) const;
    double normalizeHistogram(std::vector<double>& histogram) const;
    
    int width_;
    int height_;
    int octaveTotal_;
    int minOctaveIndex_;
    int levelTotal_;
    int gaussianLevelTotal_;
    
    double nomialSigma_;
    double baseSigma_;
    double levelSigma_;
    double deltaSigma_;
    
    int currentOctaveIndex_;
    int octaveWidth_;
    int octaveHeight_;
    std::vector< Image<float> > gaussianOctave_;
    std::vector< Image<float> > dogOctave_;
    std::vector< Image<float> > gradientMagnitudeOctave_;
    std::vector< Image<float> > gradientOrientationOctave_;

    double peakThreshold_;
    double edgeThreshold_;
    double normThreshold_;
    double magnificationFactor_;
    double windowSize_;
};
    
void readSiftFile(const std::string filename,
                  std::vector<Keypoint>& keypoints,
                  std::vector< std::vector<unsigned char> >& descriptors);
void writeSiftFile(const std::string filename,
                   const std::vector<Keypoint>& keypoints,
                   const std::vector< std::vector<unsigned char> >& descriptors);

}

#endif
