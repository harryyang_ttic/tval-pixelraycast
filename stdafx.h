#ifndef STDAFX_H
#define STDAFX_H

#include <eigen3/Eigen/Dense>

#include <omp.h>
#include "dirent.h"
#include "string.h"
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <queue>
#include <fstream>
#include <iostream>
#include <revlib.h>
#include "cmdline.h"

using namespace Eigen;
using namespace std;

#endif
