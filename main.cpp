#include"stdafx.h"
#include"Helper.h"
#include"ClosestSegment.h"

const double EPS=1e-6;

string velodyne_path, calibration_file, disparity_path;
string baseline_ratio_file, project_matrix_file, initial_matrix_file;
double baseline, baseline_ratio;
int left_crop,right_crop,thresh;
double fx,fy,ox,oy;
double SCORE_PIXEL;

bool USE_LIDAR, DISCARD_NO_HIT;

double save_thresh;
const int soft_thresh=15;
const double max_point_dist=80;

inline VectorXf Initialize(int n, int* a=NULL)
{
    VectorXf v(n);
    for(int i=0;i<n;i++)
    {
        if(a!=NULL)
            v[i]=a[i];
        else
            v[i]=0;
    }
    return v;
}

void ParseArguments(int argc, char* argv[])
{
    cmdline::parser commandParser;
    commandParser.add<string>("baseline_ratio", 'r', "baseline_ratio_file", false, "");
    commandParser.add<string>("project_matrix", 'p', "project_matrix_file", false, "");
    commandParser.add<double>("baseline", 'b', "baseline value", false, 0.558253);
    commandParser.add<int>("left_crop", 'l', "left crop value", false, 150);
    commandParser.add<int>("right_crop", 's', "right crop value", false, 50);
    commandParser.add<int>("threshold",'t',"threshold value",false,10);
    commandParser.add<bool>("use_lidar",'u',"use ground truth lidar",false,true);
    commandParser.add<bool>("discard_no_hit",'d',"discard points that do not hit anything", false,true);
    commandParser.add<string>("initial_matrix",'i',"intial matrix file that decides which point to score", false, "");
    commandParser.add<double>("pixel_error",'e',"what is the number of error pixels to score?",false,3);
    commandParser.add<double>("save_clean_threshold",'c',"what is the inlier distance you want to save for optimization purpose?",false,0.15);
    commandParser.add("verbose", 'v', "verbose");
    commandParser.add("help", 'h', "display this message");
    commandParser.set_program_name("castray");
    commandParser.footer("velo_dir, disparity_path, calib_matrix_file");
    bool isCorrectCommandline = commandParser.parse(argc, argv);
	// Check arguments
	if (!isCorrectCommandline) {
		std::cerr << commandParser.error() << std::endl;
	}
	if (!isCorrectCommandline || commandParser.exist("help") || commandParser.rest().size() < 3) {
		std::cerr << commandParser.usage() << std::endl;
		exit(1);
	}

	velodyne_path=commandParser.rest()[0];
	disparity_path=commandParser.rest()[1];
	calibration_file=commandParser.rest()[2];

    baseline_ratio_file=commandParser.get<string>("baseline_ratio");
    project_matrix_file=commandParser.get<string>("project_matrix");

    baseline=commandParser.get<double>("baseline");
    left_crop=commandParser.get<int>("left_crop");
    right_crop=commandParser.get<int>("right_crop");
    thresh=commandParser.get<int>("threshold");

    USE_LIDAR=commandParser.get<bool>("use_lidar");
    DISCARD_NO_HIT=commandParser.get<bool>("discard_no_hit");

    SCORE_PIXEL=commandParser.get<double>("pixel_error");
    save_thresh=commandParser.get<double>("save_clean_threshold");

    initial_matrix_file=commandParser.get<string>("initial_matrix");
    if(initial_matrix_file=="")
    {
        initial_matrix_file=calibration_file;
    }
}

Vector3f GetOrigin(Matrix4f M_v_c)
{
    VectorXf origin;
    int a[4]={0,0,0,1};
    origin=Initialize(4,a);
    origin = M_v_c * origin;
    Vector3f ray_ori;
    ray_ori(0) = origin(0);
    ray_ori(1) = origin(1);
    ray_ori(2) = origin(2);
    std::cout << "ray origin: " << ray_ori(0) << " " << ray_ori(1) << " " << ray_ori(2) << std::endl;
    return ray_ori;
}

ostream& operator<<(ostream& os, const Vector3f vec)
{
    os<<vec(0)<<" "<<vec(1)<<" "<<vec(2);
    return os;
}

//compare functions
inline void ComparePoints(std::vector<Vector3f > laser_in_camera, std::vector<Vector3f > intersections, std::vector<VectorXf > disparity_planes, vector<VectorXf> laser_points, double& threepixelaccuracy, double& threepixelaccuracy2, MatrixXf M_v_c)
{
    ofstream myfile("outlier.txt");
    int num=laser_in_camera.size();
    int fiveperc=0,tenperc=0,totalpts=0,threepixel=0, threepixel2=0;
    cout<<laser_in_camera.size()<<" "<<intersections.size()<<endl;
    for(int i=0;i<num;i++)
    {
        if(!(laser_in_camera[i][0]==0 && laser_in_camera[i][1]==0 && laser_in_camera[i][2]==0))
        {
            totalpts++;
            //cout<<"total:"<<totalpts<<endl;
            double dist=(laser_in_camera[i][0]-intersections[i][0])*(laser_in_camera[i][0]-intersections[i][0])
            +(laser_in_camera[i][1]-intersections[i][1])*(laser_in_camera[i][1]-intersections[i][1])
            +(laser_in_camera[i][2]-intersections[i][2])*(laser_in_camera[i][2]-intersections[i][2]);
            dist=sqrt(dist);
            double length=laser_in_camera[i][0]*laser_in_camera[i][0]
            +laser_in_camera[i][1]*laser_in_camera[i][1]
            +(laser_in_camera[i][2])*(laser_in_camera[i][2]);
            length=sqrt(length);
           // cout<<length<<endl;
            double ratio=dist/length;
            if(ratio<0.05)
                fiveperc++;
            if(ratio<0.1)
                tenperc++;

            double X=laser_in_camera[i][0], Y=laser_in_camera[i][1], Z=laser_in_camera[i][2];
            double M1=disparity_planes[i][0], M2=disparity_planes[i][1], M3=disparity_planes[i][3];
           // cout<<M1<<" "<<M2<<" "<<M3<<endl;
           // cout<<X<<" "<<Y<<" "<<Z<<endl;
            double d_predict=fx*baseline/(baseline_ratio*Z);
            double e_predict=M1*(fx*X/Z+ox)+M2*(fy*Y/Z+oy)+M3;
            if(fabs(d_predict-e_predict)<=3)
            {
                threepixel++;
            }

            Vector4f intersection_in_velodyne;
            intersection_in_velodyne(0)=intersections[i][0];
            intersection_in_velodyne(1)=intersections[i][1];
            intersection_in_velodyne(2)=intersections[i][2];
            intersection_in_velodyne(3)=1;
            intersection_in_velodyne=M_v_c.inverse()*intersection_in_velodyne;
            double dist2=fabs(intersection_in_velodyne(0)-laser_points[i][0]);
            if(dist2/(laser_points[i][0]*laser_points[i][0])<SCORE_PIXEL/(fx*baseline))
                threepixel2++;
            else
            {
                myfile<<intersection_in_velodyne(0)<<" "<<laser_points[i](0)<<endl;
            }
        }
    }
    myfile.close();
    std::cout<<"total points: "<<totalpts<<std::endl;
    threepixelaccuracy=(double)threepixel/totalpts;
    threepixelaccuracy2=(double)threepixel2/totalpts;
    std::cout<<"error: "<<" "<<(double)fiveperc/(double)totalpts<<" "<<(double)tenperc/(double)totalpts<<" "<<threepixelaccuracy<<" "<<threepixelaccuracy2<<std::endl;
}

void ReadData(vector<vector<VectorXf> >& laser_points_all, vector<MatrixXf>& disparity_all, vector<string>& point_name, int& ROW, int& COL)
{
    DIR* VelodyneDir = opendir(velodyne_path.c_str());
    if (!VelodyneDir)
    {
        cout<<"opening velodyne folder error"<<endl;
        exit(-1);
    }

    laser_points_all.clear();
    disparity_all.clear();
    point_name.clear();

    int velId=0;
    struct dirent* vFile;

    ROW=0;
    COL=0;
    while ((vFile = readdir(VelodyneDir)) != NULL)
    {
        if (strcmp(vFile->d_name, ".") == 0) continue;
        if (strcmp(vFile->d_name, "..") == 0) continue;
        velId++;

        cout << "Opening laser file: " << vFile->d_name << std::endl;
        string fullpath=velodyne_path+string(vFile->d_name);
        vector<VectorXf> laser_point = parseVelodynefile(fullpath.c_str());
        laser_points_all.push_back(laser_point);
        cout << "parse velodyne file finished." << std::endl;

        //load raw disparity
        fullpath=disparity_path+string(vFile->d_name);
        fullpath=fullpath.substr(0,fullpath.length()-10);
        string disparityPath=fullpath+"_seg_disparity.png";
        MatrixXf disparity=loadDisparityFile(disparityPath.c_str());
        disparity_all.push_back(disparity);
        cout << "load disparity finished"<<endl;

        if (ROW==0 && COL==0){
            ROW = disparity.rows();
            COL = disparity.cols();
        }
        else
        {
            if (ROW!=disparity.rows() || COL!=disparity.cols())
            {
                std::cout << "error: image size does not match!" << std::endl;
            }
        }

        point_name.push_back(string(vFile->d_name));
    }
    closedir(VelodyneDir);
}

float ConvertDisparity(float current_disparity)
{
    const float DISPARITY_FACTOR=256;
    float real_disparity=current_disparity/DISPARITY_FACTOR;
    float Z=(baseline/baseline_ratio)*fx/real_disparity;
    return Z;
}

//bilinear interpolation
float GetDepth(MatrixXf disparity, float h, float w)
{
    int row=disparity.rows(), col=disparity.cols();
    int sampleH[2]={floor(h),ceil(h)},sampleW[2]={floor(w),ceil(w)};
    float total_disparity=0;
    for(int i=0;i<2;i++)
    {
        for(int j=0;j<2;j++)
        {
            int currentH=sampleH[i],currentW=sampleW[j];
            if(currentH>=0 && currentH<row && currentW>=0 && currentW<col)
            {
                float current_disparity=disparity(currentH,currentW);
                total_disparity+=current_disparity*(1-fabs(w-currentW))*(1-fabs(h-currentH));
            }
        }
    }
    float depth=ConvertDisparity(total_disparity);
    return depth;
}

bool CompareDist(float intersection_in_velodyne, float laser_point)
{
     double dist2=fabs(intersection_in_velodyne-laser_point);
     if(dist2/(laser_point*laser_point)<SCORE_PIXEL/(fx*baseline))
        return true;
    return false;
}

float Rasterize(Vector3f point_in_camera, Vector3f ray_ori, Matrix3f M_proj, MatrixXf disparity, int height, int width, float& last_depth_res)
{
    const float DISPARITY_FACTOR=256;

    Vector3f U_in=M_proj*point_in_camera, O_in=M_proj*ray_ori;

    double X_in=U_in(0)/U_in(2), Y_in=U_in(1)/U_in(2);

    double XO_in=O_in(0)/O_in(2),YO_in=O_in(1)/O_in(2);
    Vector2f U_in2d(X_in,Y_in), O_in2d(XO_in,YO_in);

    Vector3f ray = point_in_camera-ray_ori;
    Vector3f ray_unit=ray/ray.norm();

    Vector2f ray_2d= U_in2d-O_in2d;
    Vector2f image_unit=ray_2d/ray_2d.norm();

    double ratio=ray.norm()/ray_2d.norm();
    int n=0;

    float last_depth=-1;
    float cur_depth=-1;
    float last_diff=-1;
    while(true)
    {
        //cout<<n<<endl;
        n++;
        Vector2f pos_2d=O_in2d+n*image_unit;

        Vector3f pos_3d=ray_ori+n*ray_unit*ratio;
        int image_x=pos_2d(0), image_y=pos_2d(1);
        if(image_x<0 || image_x>=width || image_y<0 || image_y>=height)
            break;
        cur_depth=(baseline/baseline_ratio)*fx/(disparity(image_y,image_x)/DISPARITY_FACTOR);

        //float cur_depth=GetDepth(disparity,image_y,image_x);
        if(pos_3d(2)>cur_depth)
        {
            float cur_diff=pos_3d(2)-cur_depth;
            cur_depth=cur_depth*last_diff/(cur_diff+last_diff)+last_depth*cur_diff/(cur_diff+last_diff);
            break;
        }
        last_depth=cur_depth;
        last_diff=cur_depth-pos_3d(2);
    }
    last_depth_res=last_depth;
    //cout<<last_depth<<" "<<cur_depth<<" "<<point_in_camera(2)<<endl;
    return cur_depth;
}

Vector3f GetTransformPt(Matrix4f M, VectorXf P)
{
    VectorXf X = M*P;
    Vector3f X_res;
    X_res(0)=X(0);
    X_res(1)=X(1);
    X_res(2)=X(2);
    return X_res;
}

int main(int argc, char *argv[])
{
    ParseArguments(argc, argv);
	omp_set_num_threads(omp_get_max_threads());
    Matrix4f M_v_c=ReadVCMatrix(calibration_file.c_str());
    baseline_ratio=ReadBaselineRatio(baseline_ratio_file.c_str());
    Matrix3f M_Proj=ReadProjMatrix(project_matrix_file.c_str(), fx, fy, ox, oy);
    Vector3f ray_ori=GetOrigin(M_v_c);
    Matrix4f M_init=ReadInitialMatrix(initial_matrix_file.c_str());

    vector<vector<VectorXf> > laser_points_all;
    vector<MatrixXf> disparity_all;
    vector<string> point_name;

    int ROW,COL;
    ReadData(laser_points_all,disparity_all,point_name,ROW,COL);

    unsigned int total_points = 0;
    for (unsigned i=0; i<laser_points_all.size(); i++)
        total_points += laser_points_all[i].size();

    vector<double> threepixels;

    #pragma omp parallel for
    for (unsigned vel=0; vel<laser_points_all.size(); vel++)
    {
        int ptnum=laser_points_all[vel].size();
        MatrixXf cur_disparity=disparity_all[vel];
        int num=0;
        int totalNum=0;
        int invalid_num=0;
        int big_num=0;
        for (int p = 0; p < ptnum; p++)
        {
            if (p % 10000 == 0) std::cout << "." << std::flush;
            VectorXf point = laser_points_all[vel][p];

            if (point(0) == 0 && point(1) == 0 && point(2) == 0)
                continue;

            VectorXf X(4);
            X(0) = point(0);
            X(1) = point(1);
            X(2) = point(2);
            X(3) = 1;

            Vector3f X_init=GetTransformPt(M_init,X);
            Vector3f UV_init=M_Proj*X_init;
            float u_init=UV_init(0)/UV_init(2), v_init=UV_init(1)/UV_init(2);
            if(u_init-1<=left_crop || u_init>=COL-right_crop || v_init-1<=0 || v_init>=ROW-1 || X_init(2)<=0)
                continue;

            Vector3f X_trans=GetTransformPt(M_v_c,X);

            totalNum++;
            float last_depth_res;
            float depth_res=Rasterize(X_trans,ray_ori,M_Proj,cur_disparity,ROW,COL,last_depth_res);

            if(depth_res==-1)
                invalid_num++;
            else if (CompareDist(depth_res,X_trans(2)))
                num++;
            else if(CompareDist(last_depth_res,X_trans(2)))
                num++;
            //else if(last_depth_res>X_trans(2))
             //   big_num++;
            //else if(X_trans(2)<10 && fabs(last_depth_res-X_trans(2))<=1)
             //   num++;
           else
                cout<<depth_res<<" "<<X_trans(2)<<endl;
        }
        double accuracy=(double)num/totalNum;
        cout<<"stat: "<<num<<" "<<invalid_num<<" "<<totalNum<<" "<<big_num<<endl;
        cout<<"accuracy: "<<accuracy<<endl;
    } // end of loop for different files
    return 0;
}




